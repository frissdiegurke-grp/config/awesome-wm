require("awful.autofocus")
local wibox = require("wibox")
local gears = require("gears")
local vicious = require("vicious")

separator = wibox.widget.imagebox()
separator:set_image(beautiful.widget_sep)

-- Menu
launcher = awful.widget.launcher({ image = beautiful.icon_awesome, menu = menu })

-- CPU temperature
if thermal then
  cpuicon = wibox.widget.imagebox()
  cpuicon:set_image(beautiful.widget_cpu)
  tzswidget = wibox.widget.textbox()
  vicious.register(tzswidget, vicious.widgets.thermal, " $1 °C", 19, thermal)
end

-- Battery state
if battery then
  batwidget = wibox.widget.textbox()
  vicious.register(batwidget, vicious.widgets.bat, "$1$2%", 30, battery)
end

-- Memory usage
if memory then
  memicon = wibox.widget.imagebox()
  memicon:set_image(beautiful.widget_mem)
  memwidget = wibox.widget.textbox()
  vicious.register(memwidget, vicious.widgets.mem, " $1%  ( $2<small> </small>MB ) ", 15)
end

-- Network usage
if traffic_device then
  dnicon = wibox.widget.imagebox()
  upicon = wibox.widget.imagebox()
  dnicon:set_image(beautiful.widget_net)
  upicon:set_image(beautiful.widget_netup)
  netwidget = wibox.widget.textbox()
  vicious.register(
    netwidget,
    vicious.widgets.net,
    '<span color="' .. beautiful.fg_netdn_widget .. '">${' .. traffic_device .. ' down_kb}</span>'
    .. '  '
    .. '<span color="' .. beautiful.fg_netup_widget ..'">${' .. traffic_device .. ' up_kb}</span>',
    3
  )
end

-- Volume level
volicon = wibox.widget.imagebox()
volicon:set_image(beautiful.widget_vol)
volwidget = wibox.widget.textbox()

function update_volume(widget)
  local stream = io.popen(volume)
  local status = stream:read("*all")
  stream:close()
  local vStr = string.match(status, "(%d?%d?%d)%%")
  local vNum = tonumber(vStr)
  if vNum then
    local volume = vNum / 100
    if volume > 1 then volume = 1 end
    if volume < 0 then volume = 0 end
    status = string.match(status, "%[(o[^%]]*)%]")
    if string.find(status, "on", 1, true) then
      volume = " <span color='black' background='white'> " .. string.format("%3d", volume * 100) .. "%<small> </small></span>"
    else
      volume = " <span color='red' background='black'>  Mute<small> </small></span>"
    end
    widget:set_markup(volume)
  end
end

update_volume(volwidget)

mytimer = gears.timer({ timeout = 3 })
mytimer:connect_signal("timeout", function () update_volume(volwidget) end)
mytimer:start()

volwidget:buttons(awful.util.table.join(
  awful.button({}, 1,
    function ()
      awful.spawn("audio m")
      update_volume(volwidget)
    end
  ),
  awful.button({}, 4,
    function ()
      awful.spawn("audio +")
      update_volume(volwidget)
    end
  ),
  awful.button({}, 5,
    function ()
      awful.spawn("audio -")
      update_volume(volwidget)
    end
  )
))
volicon:buttons(volwidget:buttons())

-- Date and time
dateicon = wibox.widget.imagebox()
dateicon:set_image(beautiful.widget_date)
datewidget = wibox.widget.textbox()
vicious.register(datewidget, vicious.widgets.date, "%D %R:%S", 1)

--
-- Create a wibox for each screen and add it
--

mywibox = {}
promptbox = {}
layoutbox = {}

taglist = {}
taglist.buttons = awful.util.table.join(
  awful.button({      }, 1, awful.tag.viewonly                                         ),
  awful.button({ mkey }, 1, awful.client.movetotag                                     ),
  awful.button({      }, 3, awful.tag.viewtoggle                                       ),
  awful.button({ mkey }, 3, awful.client.toggletag                                     ),
  awful.button({      }, 4, function(t) awful.tag.viewnext(awful.tag.getscreen(t)) end ),
  awful.button({      }, 5, function(t) awful.tag.viewprev(awful.tag.getscreen(t)) end )
)

tasklist = {}
tasklist.buttons = awful.util.table.join(
  awful.button({ }, 1,
    function (c)
      if c == client.focus then
        c.minimized = true
      else
        -- Without this, the following c:isvisible() makes no sense
        c.minimized = false
        if not c:isvisible() then
          awful.tag.viewonly(c:tags()[1])
        end
        -- This will also un-minimize the client, if needed
        client.focus = c
        c:raise()
      end
    end
  ),
  awful.button({ }, 2, function (c) c:kill() end),
  awful.button({ }, 3,
    function (c)
      if instance then
        instance:hide()
        instance = nil
      else
        instance = awful.menu.clients({ width=250 })
      end
    end
  ),
  awful.button({ }, 4,
    function (c)
      awful.client.focus.byidx(1)
      if client.focus then client.focus:raise() end
    end
  ),
  awful.button({ }, 5,
    function (c)
      awful.client.focus.byidx(-1)
      if client.focus then client.focus:raise() end
    end
  )
)

for s = 1, screen.count() do
  promptbox[s] = awful.widget.prompt()
  layoutbox[s] = awful.widget.layoutbox(s)
  layoutbox[s]:buttons(awful.util.table.join(
    awful.button({ }, 1, function () awful.layout.inc(layouts,  1) end),
    awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
    awful.button({ }, 4, function () awful.layout.inc(layouts,  1) end),
    awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)
  ))

  taglist[s] = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist.buttons)
  tasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, tasklist.buttons)
  mywibox[s] = awful.wibox({ screen = s, position = "top" })

  -- Create left layout
  local layout_left = wibox.layout.fixed.horizontal()
  layout_left:add(launcher)
  layout_left:add(taglist[s])
  layout_left:add(promptbox[s])
  layout_left:add(separator)

  -- Create right layout
  local layout_right = wibox.layout.fixed.horizontal()
  layout_right:add(separator)
  if battery then
    layout_right:add(baticon)
    layout_right:add(batwidget)
    layout_right:add(separator)
  end
  if thermal then
    layout_right:add(cpuicon)
    layout_right:add(tzswidget)
    layout_right:add(separator)
  end
  if memory then
    layout_right:add(memicon)
    layout_right:add(memwidget)
    layout_right:add(separator)
  end
  if traffic_device then
    layout_right:add(dnicon)
    layout_right:add(netwidget)
    layout_right:add(upicon)
    layout_right:add(separator)
  end
  layout_right:add(volicon)
  layout_right:add(volwidget)
  layout_right:add(separator)
  if s == 1 then -- systray not working for multiple screens
    layout_right:add(wibox.widget.systray())
    layout_right:add(separator)
  end
  layout_right:add(dateicon)
  layout_right:add(datewidget)
  layout_right:add(separator)
  layout_right:add(layoutbox[s])

  -- Combine all layouts
  local layout = wibox.layout.align.horizontal()
  layout:set_first(layout_left)
  layout:set_second(tasklist[s])
  layout:set_third(layout_right)

  -- Apply layout
  mywibox[s]:set_widget(layout)
end
