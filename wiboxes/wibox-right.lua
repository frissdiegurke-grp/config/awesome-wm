require("awful.autofocus")
local wibox = require("wibox")
local gears = require("gears")
local vicious = require("vicious")

separator = wibox.widget.imagebox()
separator:set_image(beautiful.widget_sep)

-- Menu
launcher = awful.widget.launcher({ image = beautiful.awesome_icon, menu = menu })

-- CPU temperature
if thermal then
  cpuicon = wibox.widget.imagebox()
  cpuicon:set_image(beautiful.widget_cpu)
  tzswidget = wibox.widget.textbox()
  vicious.register(tzswidget, vicious.widgets.thermal, " $1 °C", 19, thermal)
end

-- Battery state
if battery then
  batwidget = wibox.widget.textbox()
  vicious.register(batwidget, vicious.widgets.bat, "$1$2%", 30, battery)
end

-- Memory usage
if memory then
  memicon = wibox.widget.imagebox()
  memicon:set_image(beautiful.widget_mem)
  memwidget = wibox.widget.textbox()
  vicious.register(memwidget, vicious.widgets.mem, " $1%  ( $2<small> </small>MB ) ", 15)
end

-- Network usage
if traffic_device then
  dnicon = wibox.widget.imagebox()
  upicon = wibox.widget.imagebox()
  dnicon:set_image(beautiful.widget_net)
  upicon:set_image(beautiful.widget_netup)
  netwidget = wibox.widget.textbox()
  vicious.register(
    netwidget,
    vicious.widgets.net,
    '<span color="' .. beautiful.fg_netdn_widget .. '">${' .. traffic_device .. ' down_kb}</span>'
    .. '  '
    .. '<span color="' .. beautiful.fg_netup_widget ..'">${' .. traffic_device .. ' up_kb}</span>',
    3
  )
end

-- Volume level
volicon = wibox.widget.imagebox()
volicon:set_image(beautiful.widget_vol)
volwidget = wibox.widget.textbox()

function update_volume(widget)
  local volume_stream = io.popen("audio p")
	local mute_stream = io.popen("audio pm")
  local volume_str = volume_stream:read("*all")
	local mute_str = mute_stream:read("*all")
  volume_stream:close()
	mute_stream:close()
	local volume = tonumber(volume_str)
  if volume then
    --if volume > 100 then volume = 100 end
    if volume < 0 then volume = 0 end
		local volume_label
    if string.find(mute_str, "no", 1, true) then
      volume_label = " <span color='black' background='white'> " .. string.format("%3d", volume) .. "%<small> </small></span>"
    else
      volume_label = " <span color='red' background='black'>  Mute<small> </small></span>"
    end
    widget:set_markup(volume_label)
  end
end

update_volume(volwidget)

mytimer = gears.timer({ timeout = 3 })
mytimer:connect_signal("timeout", function () update_volume(volwidget) end)
mytimer:start()

volwidget:buttons(awful.util.table.join(
  awful.button({}, 1,
    function ()
      awful.spawn("audio m")
      update_volume(volwidget)
    end
  ),
  awful.button({}, 4,
    function ()
      awful.spawn("audio +")
      update_volume(volwidget)
    end
  ),
  awful.button({}, 5,
    function ()
      awful.spawn("audio -")
      update_volume(volwidget)
    end
  )
))
volicon:buttons(volwidget:buttons())

-- Date and time
dateicon = wibox.widget.imagebox()
dateicon:set_image(beautiful.widget_date)
datewidget = wibox.widget.textbox()
vicious.register(datewidget, vicious.widgets.date, "<small>%D %R:%S</small>", 1)

--
-- Create a wibox for each screen and add it
--

mywibox = {}
promptbox = {}
layoutbox = {}

taglist = {}
taglist.buttons = awful.util.table.join(
  awful.button({      }, 1, awful.tag.viewonly                                         ),
  awful.button({ mkey }, 1, awful.client.movetotag                                     ),
  awful.button({      }, 3, awful.tag.viewtoggle                                       ),
  awful.button({ mkey }, 3, awful.client.toggletag                                     ),
  awful.button({      }, 4, function(t) awful.tag.viewnext(awful.tag.getscreen(t)) end ),
  awful.button({      }, 5, function(t) awful.tag.viewprev(awful.tag.getscreen(t)) end )
)

tasklist = {}
tasklist.buttons = awful.util.table.join(
  awful.button({ }, 1,
    function (c)
      if c == client.focus then
        c.minimized = true
      else
        -- Without this, the following c:isvisible() makes no sense
        c.minimized = false
        if not c:isvisible() then
          awful.tag.viewonly(c:tags()[1])
        end
        -- This will also un-minimize the client, if needed
        client.focus = c
        c:raise()
      end
    end
  ),
  awful.button({ }, 2, function (c) c:kill() end),
  awful.button({ }, 3,
    function (c)
      if instance then
        instance:hide()
        instance = nil
      else
        instance = awful.menu.clients({ width=250 })
      end
    end
  ),
  awful.button({ }, 4,
    function (c)
      awful.client.focus.byidx(1)
      if client.focus then client.focus:raise() end
    end
  ),
  awful.button({ }, 5,
    function (c)
      awful.client.focus.byidx(-1)
      if client.focus then client.focus:raise() end
    end
  )
)

awful.screen.connect_for_each_screen(function(s)
  promptbox[s] = awful.widget.prompt()
  layoutbox[s] = awful.widget.layoutbox(s)
  layoutbox[s]:buttons(awful.util.table.join(
    awful.button({ }, 1, function () awful.layout.inc(layouts,  1) end),
    awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
    awful.button({ }, 4, function () awful.layout.inc(layouts,  1) end),
    awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)
  ))

  taglist[s] = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist.buttons)
  tasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, tasklist.buttons)
  mywibox[s] = awful.wibar({ screen = s, position = "right", width = beautiful.wibox_height * 2 })

  -- Create top layout
  local layout_top = wibox.layout.fixed.vertical()
  local layout_top_top = wibox.layout.flex.horizontal()
  layout_top_top:add(layoutbox[s])
  layout_top_top:add(launcher)
  layout_top:add(layout_top_top)
  layout_top:add(datewidget)

  -- Create left layout
  local layout_tasklist = wibox.layout.align.horizontal(separator, tasklist[s], separator)

  -- Create right layout
  local layout_utils_top = wibox.layout.fixed.horizontal()
  local layout_utils_bot = wibox.layout.fixed.horizontal()
  local layout_utils = wibox.layout.align.horizontal()
  layout_utils_top:add(separator)
  layout_utils_top:add(taglist[s])
  layout_utils_top:add(separator)
  layout_utils_top:add(promptbox[s])
  if s.index == 1 then -- systray not working for multiple screens
    layout_utils_bot:add(wibox.widget.systray())
    layout_utils_bot:add(separator)
  end
  if traffic_device then
    layout_utils_bot:add(dnicon)
    layout_utils_bot:add(netwidget)
    layout_utils_bot:add(upicon)
    layout_utils_bot:add(separator)
  end
  if memory then
    layout_utils_bot:add(memicon)
    layout_utils_bot:add(memwidget)
    layout_utils_bot:add(separator)
  end
  if thermal then
    layout_utils_bot:add(cpuicon)
    layout_utils_bot:add(tzswidget)
    layout_utils_bot:add(separator)
  end
  layout_utils:set_first(layout_utils_top)
  layout_utils:set_third(layout_utils_bot)

  -- Combine left and right layout
  local layout_both = wibox.layout.flex.vertical()
  local layout_both_rotated = wibox.container.rotate()
  layout_both:add(layout_utils)
  layout_both:add(layout_tasklist)
  layout_both_rotated:set_direction("west")
  layout_both_rotated:set_widget(layout_both)

  -- Create bottom layout
  local layout_bot = wibox.layout.fixed.vertical()
  if battery then
    layout_bot:add(batwidget)
  end
  layout_bot:add(volwidget)

  -- Combine all layouts
  local layout = wibox.layout.align.vertical()
  layout:set_first(layout_top)
  layout:set_second(layout_both_rotated)
  layout:set_third(layout_bot)

  -- Apply layout
  mywibox[s]:set_widget(layout)
end)

