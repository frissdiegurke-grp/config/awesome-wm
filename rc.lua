local menubar = require("menubar")

require("utils/debug")

--
-- Variables
--

-- -- Externals
terminal = "urxvt"
terminal_exec = terminal .. " -e "
editor_cmd = terminal_exec .. "vim"
editor = "leafpad"
-- -- set following false to hide
traffic_device = "wlp2s0"
thermal = "thermal_zone1"
--thermal = false
battery = "BAT0"
--battery = false
memory = true

-- -- Config
awful = require("awful")
beautiful = require("beautiful")
wibox = "right"
theme = "darkblue"
tagsAmount = 9
sloppyFocus = false
slaveClient = false
wallpaper = true
desktopHandling = true
keyboardMouseControl = true

-- -- Internals
mkey = "Mod4"
akey = "Mod1"
ckey = "Control"
skey = "Shift"
backspacekey = "BackSpace"
returnkey = "Return"
spacekey = "space"
escapekey = "Escape"
homekey = "Home"
endkey = "End"
pausekey = "Pause"
tabkey = "Tab"
deletekey = "Delete"
printkey = "Print"
volumeinckey = "XF86AudioRaiseVolume"
volumedeckey = "XF86AudioLowerVolume"
volumemutekey = "XF86AudioMute"
calculatorkey = "XF86Calculator"
touchpadkey = "XF86TouchpadToggle"
brightnessinckey = "XF86MonBrightnessUp"
brightnessdeckey = "XF86MonBrightnessDown"
kbdinckey = "XF86KbdBrightnessUp"
kbddeckey = "XF86KbdBrightnessDown"

hotkeys_popup = require("awful.hotkeys_popup").widget

--
-- Initialize other files
--

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it

-- -- theme
beautiful.init(awful.util.get_themes_dir() .. theme .. "/theme.lua")

-- -- utils (files that ain't interesting to customize)
require("utils/tags")
require("utils/spawn_once")
require("utils/signals")
--require("utils/screen")

-- -- misc scripts
require("menu")
require("desktop")
require("keybindings")
require("rules")

-- -- wibox
require("wiboxes/wibox-" .. wibox)

-- -- autostarts
require("autostart")

