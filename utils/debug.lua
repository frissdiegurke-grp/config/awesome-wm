local naughty = require("naughty")

-- Error handling
-- -- Check if awesome encountered an error during startup and fell back to
-- -- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
  naughty.notify({
    preset = naughty.config.presets.critical,
    title = "Oops, there were errors during startup!",
    timeout = 6,
    text = awesome.startup_errors
  })
end

-- Handle runtime errors after startup
do
  local in_error = false
  awesome.connect_signal("debug::error",
    function (err)
      -- Make sure we don't go into an endless error loop
      if in_error then return end
      in_error = true
      naughty.notify({
        preset = naughty.config.presets.critical,
        title = "Oops, an error happened!",
        timeout = 6,
        text = err
      })
      in_error = false
    end
  )

	awesome.connect_signal("debug::deprecation",
		function (err)
			if in_error then return end
			in_error = true
			naughty.notify({
				bg = "#AAAA00",
				fg = "#333300",
				timeout = 5,
				text = err
			})
			in_error = false
		end
	)
end