local awful = require("awful")

-- Table of layouts to cover with awful.layout.inc, order matters.
layouts = {
  awful.layout.suit.tile,
  awful.layout.suit.tile.left,
  awful.layout.suit.tile.bottom,
  awful.layout.suit.tile.top,
  --awful.layout.suit.fair,
  --awful.layout.suit.fair.horizontal,
  --awful.layout.suit.spiral,
  --awful.layout.suit.spiral.dwindle,
  --awful.layout.suit.max,
  --awful.layout.suit.max.fullscreen,
  --awful.layout.suit.magnifier,
  --awful.layout.suit.corner.ne,
  --awful.layout.suit.corner.sw,
  --awful.layout.suit.corner.se,
  awful.layout.suit.floating
}

-- Tags

-- -- Define a tag table which hold all screen tags.
tags = {
  names = {},
  layouts = {
    layouts[1],
    layouts[1],
    layouts[1],
    layouts[1],
    layouts[1],
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile,
    layouts[1],
    awful.layout.suit.tile.bottom
  },
  factors = {0,0,0,0,0,0,0.2,0,0.35}
}
for i = 1,tagsAmount do
  tags.names[i] = i
end

-- -- register and configure tags
awful.screen.connect_for_each_screen(function(s)
  -- add tags for screen
  tags[s] = awful.tag(tags.names, s, tags.layouts)
  -- incmwfact of given factors
  for i = 1,tagsAmount do
		tags[s][i]:view_only()
    awful.tag.incmwfact(tags.factors[i])
  end
  -- display tag#1 on startup
  tags[s][1]:view_only()
end)

