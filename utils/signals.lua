-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c, startup)
  -- Enable sloppy focus
  if sloppyFocus then
    c:connect_signal("mouse::enter",
      function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
          and awful.client.focus.filter(c) then
          client.focus = c
        end
      end
    )
  end

  if not startup then
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    if slaveClient then
      awful.client.setslave(c)
    end

    -- Put windows in a smart way, only if they does not set an initial position.
    if not c.size_hints.user_position and not c.size_hints.program_position then
      awful.placement.no_overlap(c)
      awful.placement.no_offscreen(c)
    end
  end
end)

-- Signal function to execute when a client gains focus
client.connect_signal("focus", function(c) c.opacity = 1; c.border_color = beautiful.border_focus end)

-- Signal function to execute when a client loses focus
client.connect_signal("unfocus", function(c) c.opacity = 0.7; c.border_color = beautiful.border_normal end)