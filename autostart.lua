local awful = require("awful")

awful.spawn.with_shell("systemctl --user start \"awesome@${DISPLAY}.target\"")

awesome.connect_signal("exit", function()
  awful.spawn.with_shell("systemctl --user stop \"awesome@${DISPLAY}.target\"")
end)

-- TODO create systemd service files instead

spawn_once("unclutter --timeout 2")
spawn_once("spacefm -d", {match=true})

awful.spawn("xset -dpms s 0")
spawn_once("urxvtd")

