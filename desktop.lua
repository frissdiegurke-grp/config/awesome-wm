local gears = require("gears")

if desktopHandling then

  root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () menu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
  ))

end

if wallpaper then

  local function set_wallpaper(s)
    if beautiful.wallpaper then
      local wallpaper = beautiful.wallpaper
      if type(wallpaper) == "function" then wallpaper = wallpaper(s) end
      gears.wallpaper.maximized(wallpaper, s, true)
    end
  end

  screen.connect_signal("property::geometry", set_wallpaper)

  awful.screen.connect_for_each_screen(set_wallpaper)

end