local awful = require("awful")
awful.rules = require("awful.rules")

clientbuttons = awful.util.table.join(
    awful.button({      }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ mkey }, 1, awful.mouse.client.move),
    awful.button({ mkey }, 3, awful.mouse.client.resize))

local function preferred_tag(c)
	if not awesome.startup then c:tags({awful.screen.focused().selected_tag}) end
end

awful.rules.rules = {
  -- Use xprop or xwininfo to gain information of windows
	-- WM_CLASS first entry is "instance", second entry is "class"

  -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
										 raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
						    		 screen = awful.screen.preferred,
										 fullscreen = false,
										 maximized = false,
										 maximized_horizontal = false,
										 maximized_vertical = false,
						         placement = awful.placement.no_overlap+awful.placement.no_offscreen },
			--callback = preferred_tag },
		},

  -- Custom rules

  -- Chrom[ium|e] fix open in current tag (since v52)
    { rule_any = { class = { "Chromium", "Google-chrome" } },
		  callback = function(c) if not awesome.startup then c:tags({awful.screen.focused().selected_tag}) end end },

    { rule_any = { instance = { "crx_bikioccmkafdpakkkcpdbppfkghcmihk", "xchat" } },
      callback = function(c) if not awesome.startup then c:tags({awful.screen.focused().tags[6]}) end end },

    { rule_any = { class = { "Claws-mail" } },
      callback = function(c) if not awesome.startup then c:tags({awful.screen.focused().tags[9]}) end end },

    { rule_any = { name = { "nodekey", "newsboat" } }, properties = { raise = false },
      callback = function(c)
		    if not awesome.startup then
				  c:tags({awful.screen.focused().tags[9]})
					awful.client.setslave(c)
				end
			end },

    { rule = { instance = "audacious" },
      callback = function(c) if not awesome.startup then c:tags({awful.screen.focused().tags[8]}) end end },

    { rule = { class = "Steam" },
			properties = { floating = false },
      callback = function(c) if not awesome.startup then c:tags({awful.screen.focused().tags[7]}) end end },

    { rule_any = { class = { "jetbrains-idea", "jetbrains-webstorm", "jetbrains-clion" } },
      callback = function(c) if not awesome.startup then c:tags({awful.screen.focused().tags[1]}) end end },

}
