local awful = require("awful")
local menubar = require("menubar")

-- {{{ Key bindings
globalkeys = awful.util.table.join(
  -- Terminal
  awful.key({ mkey }, returnkey, function () awful.spawn(terminal) end, {group="common", description="open terminal"}),

	-- Help
	awful.key({ mkey }, "slash", hotkeys_popup.show_help, {group="awesome", description="show help"}),

  -- XF86 Keys
  -- -- Volume
  awful.key({      }, volumeinckey,  function () awful.spawn("audio + + +") end, {group="special", description="in/decrease volume by 3%"}),
  awful.key({      }, volumedeckey,  function () awful.spawn("audio - - -") end, {group="special", description="in/decrease volume by 3%"}),
  awful.key({ ckey }, volumeinckey,  function () awful.spawn("audio +")     end, {group="special", description="in/decrease volume by 1%"}),
  awful.key({ ckey }, volumedeckey,  function () awful.spawn("audio -")     end, {group="special", description="in/decrease volume by 1%"}),
  awful.key({      }, volumemutekey, function () awful.spawn("audio m")     end, {group="special", description="toggle mute"}),
  awful.key({ mkey       }, "Prior", function () awful.spawn("audio + + +") end, {group="media", description="in/decrease volume by 3%"}),
  awful.key({ mkey       }, "Next",  function () awful.spawn("audio - - -") end, {group="media", description="in/decrease volume by 3%"}),
  awful.key({ mkey, akey }, "Prior", function () awful.spawn("audio +")     end, {group="media", description="in/decrease volume by 1%"}),
  awful.key({ mkey, akey }, "Next",  function () awful.spawn("audio -")     end, {group="media", description="in/decrease volume by 1%"}),
  awful.key({ mkey       }, "Pause", function () awful.spawn("audio m")     end, {group="media", description="toggle mute"}),
  -- -- Calculator
  awful.key({      }, calculatorkey, function () awful.spawn(terminal_exec .. "bc -l") end, {group="special", description="open calculator"}),
  -- -- Touchpad
  awful.key({      }, touchpadkey,   function () awful.spawn("touchpad") end, {group="special", description="toggle touchpad disable"}),
  -- -- Brightness
  awful.key({      }, brightnessinckey, function () awful.spawn("sudo xblight +") end, {group="special", description="in/decrease backlight"}),
  awful.key({      }, brightnessdeckey, function () awful.spawn("sudo xblight -") end, {group="special", description="in/decrease backlight"}),
  awful.key({ skey }, brightnessinckey, function () awful.spawn("sudo xblight 1") end, {group="special", description="set backlight 1/0"}),
  awful.key({ skey }, brightnessdeckey, function () awful.spawn("sudo xblight 0") end, {group="special", description="set backlight 1/0"}),
  -- -- Keyboard backlight
  awful.key({      }, kbdinckey,        function () awful.spawn("asus-kbd-backlight up") end, {group="special", description="in/decrease keyboard backlight"}),
  awful.key({      }, kbddeckey,        function () awful.spawn("asus-kbd-backlight down") end, {group="special", description="in/decrease keyboard backlight"}),

  -- Screen Control
  --awful.key({ mkey       }, "s", xrandrNext, {group="screen", description="next monitor configuration"}),
  --awful.key({ mkey, skey }, "s", xrandrPrev, {group="screen", description="prev monitor configuration"}),

  -- Screenshots
  awful.key({      }, printkey,  function () awful.spawn("scrot")    end, {group="screen", description="make screenshot"}),
  awful.key({ akey }, printkey,  function () awful.spawn("scrot -s") end, {group="screen", description="make screenshot (selection)"}),
  awful.key({ mkey }, printkey,  function () awful.spawn("scrot -u") end, {group="screen", description="make screenshot (focused)"}),

  -- Umlauts
  awful.key({ mkey, akey, ckey       }, "a",  function () awful.spawn.with_shell("umlaut a | xclip -selection primary && umlaut a | xclip -selection clipboard") end, {group="umlauts", description="copy ä/ö/ü/ß"}),
  awful.key({ mkey, akey, ckey       }, "o",  function () awful.spawn.with_shell("umlaut o | xclip -selection primary && umlaut o | xclip -selection clipboard") end, {group="umlauts", description="copy ä/ö/ü/ß"}),
  awful.key({ mkey, akey, ckey       }, "u",  function () awful.spawn.with_shell("umlaut u | xclip -selection primary && umlaut u | xclip -selection clipboard") end, {group="umlauts", description="copy ä/ö/ü/ß"}),
  awful.key({ mkey, akey, ckey       }, "s",  function () awful.spawn.with_shell("umlaut s | xclip -selection primary && umlaut s | xclip -selection clipboard") end, {group="umlauts", description="copy ä/ö/ü/ß"}),
  awful.key({ mkey, akey, ckey, skey }, "a",  function () awful.spawn.with_shell("umlaut A | xclip -selection primary && umlaut A | xclip -selection clipboard") end, {group="umlauts", description="copy Ä/Ö/Ü/ẞ"}),
  awful.key({ mkey, akey, ckey, skey }, "o",  function () awful.spawn.with_shell("umlaut O | xclip -selection primary && umlaut O | xclip -selection clipboard") end, {group="umlauts", description="copy Ä/Ö/Ü/ẞ"}),
  awful.key({ mkey, akey, ckey, skey }, "u",  function () awful.spawn.with_shell("umlaut U | xclip -selection primary && umlaut U | xclip -selection clipboard") end, {group="umlauts", description="copy Ä/Ö/Ü/ẞ"}),
  awful.key({ mkey, akey, ckey, skey }, "s",  function () awful.spawn.with_shell("umlaut S | xclip -selection primary && umlaut S | xclip -selection clipboard") end, {group="umlauts", description="copy Ä/Ö/Ü/ẞ"}),

  -- Tag-Navigation
  awful.key({ mkey }, "i", awful.tag.viewnext, {group="tags", description="view next/previous"}),
  awful.key({ mkey }, "u", awful.tag.viewprev, {group="tags", description="view next/previous"}),

  -- Focus-Navigation
  awful.key({ mkey }, "j",
    function ()
      awful.client.focus.byidx( 1)
      if client.focus then client.focus:raise() end
    end, {group="client", description="focus next/previous by index"}),
  awful.key({ mkey }, "k",
    function ()
      awful.client.focus.byidx(-1)
      if client.focus then client.focus:raise() end
    end, {group="client", description="focus next/previous by index"}),

  -- X-Kill
  awful.key({ mkey }, "x", function () awful.spawn("xkill") end, {group="client", description="select client to kill"}),

  -- Layout manipulation
  awful.key({ mkey, skey }, "j", function () awful.client.swap.byidx(  1)    end, {group="client", description="swap client by index"}),
  awful.key({ mkey, skey }, "k", function () awful.client.swap.byidx( -1)    end, {group="client", description="swap client by index"}),
  awful.key({ mkey, ckey }, "j", function () awful.screen.focus_relative( 1) end, {group="screen", description="focus next/previous"}),
  awful.key({ mkey, ckey }, "k", function () awful.screen.focus_relative(-1) end, {group="screen", description="focus next/previous"}),
  awful.key({ mkey,      }, tabkey,
    function ()
      awful.client.focus.history.previous()
      if client.focus then
        client.focus:raise()
      end
    end, {group="client", description="focus previous client by focus history"}),

  -- Standard program
  awful.key({ mkey, ckey }, "r",          awesome.restart, {group="awesome", description="restart (reload configuration)"}),
  awful.key({ mkey, ckey }, "q",          awesome.quit, {group="awesome", description="quit (logout)"}),

  awful.key({ mkey,      }, "l",      function () awful.tag.incmwfact( 0.05)    end), -- TODO continue descriptions
  awful.key({ mkey,      }, "h",      function () awful.tag.incmwfact(-0.05)    end),
  awful.key({ mkey, skey }, "h",      function () awful.tag.incnmaster( 1)      end),
  awful.key({ mkey, skey }, "l",      function () awful.tag.incnmaster(-1)      end),
  awful.key({ mkey, ckey }, "h",      function () awful.tag.incncol( 1)         end),
  awful.key({ mkey, ckey }, "l",      function () awful.tag.incncol(-1)         end),
  awful.key({ mkey,      }, spacekey, function () awful.layout.inc(layouts,  1) end),
  awful.key({ mkey, skey }, spacekey, function () awful.layout.inc(layouts, -1) end),

  awful.key({ mkey, skey }, "n",      awful.client.restore),

  -- Prompts
  -- -- default
  awful.key({ mkey       }, "r", function () promptbox[mouse.screen]:run() end),
  -- applications
  awful.key({ mkey       }, "p", function() menubar.show()                 end),
  -- -- lua
  awful.key({ mkey, skey }, "r",
    function ()
      awful.prompt.run({ prompt = "Run Lua code: " },
        promptbox[mouse.screen].widget,
        awful.util.eval, nil,
        awful.util.getdir("cache") .. "/history_eval")
    end),

  -- Power Management
  awful.key({ mkey             }, escapekey, function () awful.spawn("systemctl poweroff") end),
  awful.key({ mkey             }, homekey,   function () awful.spawn("systemctl reboot")   end),
  awful.key({ mkey             }, deletekey, function () awful.spawn("systemctl suspend")  end),

  -- XRandR
  awful.key({ mkey             }, "s",       xrandrNext),
  awful.key({ mkey, skey       }, "s",       xrandrPrev),
  awful.key({ mkey, akey       }, "n",       function () awful.spawn("monitor 0"); awesome.restart()                                      end),
  awful.key({ mkey, akey       }, "m",       function () awful.spawn("monitor 1"); awesome.restart()                                      end),
  awful.key({ mkey, ckey       }, "m",       function () awful.spawn("monitor 2"); awesome.restart()                                      end),
  awful.key({ mkey, akey, ckey }, "m",       function () awful.spawn("monitor 3"); awesome.restart()                                      end),
  awful.key({ mkey, akey, skey, ckey }, "m", function () awful.spawn("monitor 4"); awesome.restart()                                      end),

  -- Custom programs
  awful.key({ mkey, akey       }, "s",       function () awful.spawn.with_shell("LD_PRELOAD='/usr/$LIB/libstdc++.so.6 /usr/$LIB/libgcc_s.so.1 /usr/$LIB/libxcb.so.1 /usr/$LIB/libgpg-error.so' steam")           end),
  awful.key({ mkey             }, "e",       function () awful.spawn("spacefm -nw")     end),
  awful.key({ mkey             }, "c",       function () awful.spawn("firefox")        end),
  awful.key({ mkey, akey       }, "c",       function () awful.spawn("chromium --no-default-browser-check --alsa-output-device=default")        end),
--  awful.key({ mkey, akey       }, "c",       function () awful.spawn.with_shell("chromium --no-default-browser-check --user-data-dir=$HOME/.config/chromium/Secondary --alsa-output-device=default") end),
  awful.key({ mkey, ckey       }, "c",       function () awful.spawn("chromium --no-default-browser-check --disable-web-security --alsa-output-device=default")                                    end),
  awful.key({ mkey, akey       }, "g",       function () awful.spawn("signal-desktop") end),
  --awful.key({ mkey, akey       }, "g",       function () awful.spawn("chromium --profile-directory=Default --app-id=bikioccmkafdpakkkcpdbppfkghcmihk --alsa-output-device=default") end),
  awful.key({ mkey             }, "d",       function () awful.spawn("intellij-idea-ultimate-edition")                                     end),
  awful.key({ mkey             }, "w",       function () awful.spawn("webstorm")                                                 end),
  awful.key({ mkey, akey       }, "l",       function () awful.spawn("clion")                                                              end),
  awful.key({ mkey, ckey       }, "l",       function () awful.spawn("slock")                                                              end),
  awful.key({ mkey, ckey, skey }, "l",       function () awful.spawn.with_shell("slock systemctl suspend -i")                                                              end),
  awful.key({ mkey             }, "v",       function () awful.spawn("vlc")                                                                end),
  awful.key({ mkey             }, "b",       function () awful.spawn.with_shell("systemctl --user start claws-mail@$DISPLAY") end),
  awful.key({ mkey             }, "y",       function () awful.spawn.with_shell("systemctl --user start nodekey@$DISPLAY") end),
  awful.key({ mkey             }, "a",       function () awful.spawn("audacious")                                                          end),
  awful.key({ mkey, akey       }, "x",       function () awful.spawn("hexchat")                                                              end)
)

-- tag jumps by digit
for i = 1,tagsAmount do

  globalkeys = awful.util.table.join(globalkeys,
    awful.key({ mkey }, "#" .. i + 9,
      function ()
        local screen = mouse.screen
        local tag = screen.tags[i]
        if tag then
          tag:view_only()
        end
      end),
    awful.key({ mkey, ckey }, "#" .. i + 9,
      function ()
        local screen = mouse.screen
        local tag = screen.tags[i]
        if tag then
          awful.tag.viewtoggle(tag)
        end
      end),
    awful.key({ mkey, skey }, "#" .. i + 9,
      function ()
				if client.focus then
          local tag = client.focus.screen.tags[i]
          if tag then
						client.focus:move_to_tag(tag)
          end
		  	end
      end),
    awful.key({ mkey, ckey, skey }, "#" .. i + 9,
      function ()
				if client.focus then
          local tag = client.focus.screen.tags[i]
					if tag then
						client.focus:toggle_tag(tag)
					end
				end
      end)
  )

end

-- Set keys
root.keys(globalkeys)
-- }}}

-- Client keys (to be used within rules.lua)

clientkeys = awful.util.table.join(
  awful.key({ mkey,      }, "f",       function (c) c.fullscreen = not c.fullscreen  end),
  awful.key({ mkey, skey }, "c",       function (c) c:kill()                         end),
  awful.key({ mkey, ckey }, spacekey,  awful.client.floating.toggle                     ),
  awful.key({ mkey, ckey }, returnkey, function (c) c:swap(awful.client.getmaster()) end),
  awful.key({ mkey,      }, "o",       awful.client.movetoscreen                        ),
  awful.key({ mkey,      }, "t",       function (c) c.ontop = not c.ontop            end),
  awful.key({ mkey,      }, "n",
    function (c)
      -- The client currently has the input focus, so it cannot be
      -- minimized, since minimized clients can't have the focus.
      c.minimized = true
    end),
  awful.key({ mkey,      }, "m",
    function (c)
      c.maximized_horizontal = not c.maximized_horizontal
      c.maximized_vertical   = not c.maximized_vertical
    end)
)
